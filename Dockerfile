FROM openjdk:17.0.1-jdk-slim

WORKDIR /app

COPY . .

RUN ./mvnw clean install

EXPOSE 8080

CMD [ "java", "-jar", "target/todolist-1.0.0.jar" ]
